package ninja.jamessmith.cloudclip;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import javax.servlet.AsyncContext;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ServiceServlet handles handles all GET and POST requests from the CloudClip
 * client. A parameter named "method" is expected in the URL with any of the
 * values, "add" "connect" "disconnect" "fetch" "remove".
 * 
 * "add" adds a clip to a shared session's clip-board.
 * Required Headers: "session", "uuid"
 * Request Stream: contains the String to be added to the shared session
 * Response: only status code
 * 
 * "connect" establishes a connection with a shared session and receives all
 * of the current clips in its clip-board.
 * Required Headers: "session"
 * Response: JSON containing a randomly generated "uuid", as well as a JSON for
 * each clip in the shared session's clip-board
 * 
 * "disconnect" disconnects from the shared connection.
 * Required Headers: "session", "uuid"
 * Response: only status code
 * 
 * "fetch" requests updates for which clips have been added and removed by other
 * clients.
 * Required Headers: "session", "uuid"
 * Response: JSON containing "clip" and "method", where the method is either to
 * "add" or "remove" the String contained in "clip"
 * 
 * "remove" removes a clip from a shared session's clip-board.
 * Required Headers: "session", "uuid"
 * Request Stream: contains the String to be removed from the shared session
 * Response: only status code
 * 
 * A SessionManager is
 * used to organize the shared sessions, clip-boards, and messages to clients.
 */
@WebServlet(asyncSupported = true, urlPatterns = { "/Service" })
public class ServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String PARAMETER_METHOD = "method";
	private static final String ADD_METHOD = "add";
	private static final String CONNECT_METHOD = "connect";
	private static final String DISCONNECT_METHOD = "disconnect";
	private static final String FETCH_METHOD = "fetch";
	private static final String REMOVE_METHOD = "remove";
	
	static final String UUID_HEADER = "uuid";
	static final String SESSION_HEADER = "session";
	static final String CLIP_HEADER = "clip";
	
	private SessionManager manager;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServiceServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		this.manager = SessionManager.getInstance();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter(PARAMETER_METHOD);
		
		try {
			if (method == null) {
				response.setStatus(401);
			} else if (method.equals(CONNECT_METHOD)) {
				doConnect(request, response);
			} else if (method.equals(DISCONNECT_METHOD)) {
				doDisconnect(request, response);
			} else if (method.equals(FETCH_METHOD)) {
				doFetch(request, response);
			} else {
				response.setStatus(401);
			} 
		} catch (Exception e) {
			response.setStatus(500);
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter(PARAMETER_METHOD);
		
		if (method == null) {
			response.setStatus(401);
		}
		else if (method.equals(ADD_METHOD)) {
			doAdd(request, response);
		}
		else if (method.equals(REMOVE_METHOD)) {
			doRemove(request, response);
		}
		else {
			response.setStatus(401);
		}
	}

	private void doAdd(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String session = req.getHeader(SESSION_HEADER);
		StringBuilder sb = new StringBuilder();
		InputStreamReader in = new InputStreamReader(req.getInputStream());
		int nextChar = in.read();
		
		while (nextChar != -1) {
			sb.append((char)nextChar);
			nextChar = in.read();
		}

		sb.delete(sb.length()-2, sb.length());
		
		String uuid = req.getHeader(UUID_HEADER);
		String clip = sb.toString();
		
		if (manager.addClip(session, uuid, clip)) {
			resp.setStatus(200);
		}
		else {
			resp.setStatus(401);
		}
	}
	
	private void doConnect(HttpServletRequest req, HttpServletResponse resp) {
		String session = req.getHeader(SESSION_HEADER);
		
		if (session == null) {
			resp.setStatus(401);
		}
		else {
			try {
				manager.addSession(session, req.startAsync());
			} catch (IllegalStateException e) {
				resp.setStatus(401);
				e.printStackTrace();
			} catch (IOException e) {
				resp.setStatus(401);
				e.printStackTrace();
			}
		}
	}
	
	private void doDisconnect(HttpServletRequest req, HttpServletResponse resp) {
		String session = req.getHeader(SESSION_HEADER);
		String uuid = req.getHeader(UUID_HEADER);
		
		if (session == null || uuid == null) {
			resp.setStatus(401);
		}
		else {
			manager.removeSession(session, uuid);
		}
	}
	
	private void doFetch(HttpServletRequest req, HttpServletResponse resp) {
		String session = req.getHeader(SESSION_HEADER);
		String uuid = req.getHeader(UUID_HEADER);
		
		try {
			AsyncContext context = req.startAsync();
			context.setTimeout(0);
			manager.fetch(session, uuid, context);
		} catch (IllegalStateException e) {
			resp.setStatus(401);
			e.printStackTrace();
		} catch (IOException e) {
			resp.setStatus(401);
			e.printStackTrace();
		}
	}
	
	private void doRemove(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String session = req.getHeader(SESSION_HEADER);
		StringBuilder sb = new StringBuilder();
		InputStreamReader in = new InputStreamReader(req.getInputStream());
		int nextChar = in.read();
		
		while (nextChar != -1) {
			sb.append((char)nextChar);
			nextChar = in.read();
		}

		sb.delete(sb.length()-2, sb.length());
		
		String uuid = req.getHeader(UUID_HEADER);
		String clip = sb.toString();
		
		if (manager.removeClip(session, uuid, clip)) {
			resp.setStatus(200);
		}
		else {
			resp.setStatus(401);
		}
	}
}
