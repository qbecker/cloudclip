package ninja.jamessmith.cloudclip;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

/**
 * SessionManager organizes shared sessions, stores clip-board contents, and
 * builds messages to send to clients.
 * 
 * In the event that a fetch is called and there are no updates to send to the
 * client, the request will be handled asynchronously until a message is ready
 * to send to the client.
 * 
 * Message are stored in a BlockingQueue which is being monitored by a Thread
 * notifier. When a message is added to the queue, the notifier will analyze the
 * message and add the appropriate JSONs to the ByteArrayOutputStreams for each
 * client in the shared session. When a client is ready for updates, the server
 * writes the client's ByteArrayOutputStream to the response's OutputStream.
 *
 */
public class SessionManager {

	private static SessionManager instance;
	
	private Map<String, List<String>> clipboards;
	private Map<String, Map<String, ByteArrayOutputStream>> sessions;
	private BlockingQueue<String[]> messages; // 0=session, 1=uuid, 2=method, 3=clip	
	final private Thread notifier = new Thread(new Runnable() {

		@Override
		public void run() {
			while (true) {
				try {
					String[] message = messages.take();
					JSONObject json = new JSONObject();					
					Map<String, ByteArrayOutputStream> session = sessions.get(message[0]);
					ByteArrayOutputStream originStream = session.get(message[1]);

					json.put("method", message[2]);
					json.put("clip", message[3]);
					
					for (ByteArrayOutputStream stream : session.values()) {
						if (originStream != stream) {
							synchronized(stream) {
								PrintWriter writer = new PrintWriter(stream);
								writer.println(json.toJSONString());
								writer.flush();
							}
						}
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	});
	
	private SessionManager() {
		if (instance != null) {
			throw new RuntimeException("Creating second SessionManager");
		}
		
		clipboards = new HashMap<String, List<String>>();
		sessions = new HashMap<String, Map<String, ByteArrayOutputStream>>();
		messages = new LinkedBlockingQueue<String[]>();
		
		notifier.start();
		instance = this;
	}
	
	synchronized public static SessionManager getInstance() {
		if (instance == null) {
			new SessionManager();
		}
		
		return instance;
	}
	
	public void addSession(final String sessionKey, AsyncContext context) throws IOException {
		HttpSession session = ((HttpServletRequest)context.getRequest()).getSession();
		final String uuid = (String)session.getAttribute("uuid");
		
		if (session.getAttribute("sessionKey") != null) {
			this.removeSession((String) session.getAttribute("sessionKey"), uuid);
		}
		
		session.setAttribute("sessionKey", sessionKey);
		
		JSONObject json = new JSONObject();
		
		if (!sessions.containsKey(sessionKey)) {
			sessions.put(sessionKey, new HashMap<String, ByteArrayOutputStream>());
			clipboards.put(sessionKey, new ArrayList<String>());
		}
		
		sessions.get(sessionKey).put(uuid, new ByteArrayOutputStream());
		json.put(ServiceServlet.UUID_HEADER, uuid);
		context.getResponse().getWriter().println(json.toJSONString());
		
		for (String s : clipboards.get(sessionKey)) {
			JSONObject clip = new JSONObject();
			clip.put("method", "add");
			clip.put("clip", s);
			context.getResponse().getWriter().println(clip.toJSONString());
		}
		
		context.addListener(new AsyncListener() {
			
			@Override
			public void onComplete(AsyncEvent arg0) throws IOException {
				
			}

			@Override
			public void onError(AsyncEvent arg0) throws IOException {
				removeSession(sessionKey, uuid);
			}

			@Override
			public void onStartAsync(AsyncEvent arg0) throws IOException {
				
			}

			@Override
			public void onTimeout(AsyncEvent arg0) throws IOException {
				removeSession(sessionKey, uuid);
			}			
		});
		
		context.complete();
	}
	
	public void removeSession(String sessionKey, String uuid) {
		if (sessions.containsKey(sessionKey)) {
			Map<String, ByteArrayOutputStream> session = sessions.get(sessionKey);
			if (session.containsKey(uuid)) {
				session.remove(uuid);
				
				Logger.getAnonymousLogger().info("uuid removed from session");
			}
			if (session.isEmpty()) {
				sessions.remove(sessionKey);
				clipboards.remove(sessionKey);
				
				Logger.getAnonymousLogger().info("session cleared");
			}
		}
	}
	
	public boolean addClip(String sessionKey, String uuid, String clip) {
		boolean isValid = sessions.containsKey(sessionKey)
				&& sessions.get(sessionKey).containsKey(uuid);
		
		if (isValid) {
			List<String> clipboard = clipboards.get(sessionKey);
			
			clipboard.add(clip);
			
			String[] message = new String[4];
			message[0] = sessionKey;
			message[1] = uuid;
			message[2] = "add";
			message[3] = clip;
			
			messages.add(message);
		}

		return isValid;
	}
	
	public boolean removeClip(String sessionKey, String uuid, String clip) {
		boolean isValid = sessions.containsKey(sessionKey)
				&& sessions.get(sessionKey).containsKey(uuid);
		
		if (isValid) {
			List<String> clipboard = clipboards.get(sessionKey);
			
			clipboard.remove(clip);
			
			String[] message = new String[4];
			message[0] = sessionKey;
			message[1] = uuid;
			message[2] = "remove";
			message[3] = clip;
			
			messages.add(message);
		}
		
		return isValid;
	}
	
	public void fetch(final String sessionKey, final String uuid, final AsyncContext context) throws IOException {
		boolean isValid = sessions.containsKey(sessionKey)
				&& sessions.get(sessionKey).containsKey(uuid);

		if (isValid) {
			ByteArrayOutputStream stream = sessions.get(sessionKey).get(uuid);
			
			if (stream.size() > 0) {
				synchronized (stream) {
					stream.writeTo(context.getResponse().getOutputStream());
					stream.reset();
				}
				context.complete();
			} else {
				new Thread(new Runnable() {

					@Override
					public void run() {
						boolean isDone = false;

						while (!isDone) {
							Map<String, ByteArrayOutputStream> session = sessions.get(sessionKey);
							ByteArrayOutputStream stream = null;
							
							if (session != null) {
								stream = sessions.get(sessionKey).get(uuid);
							}
							if (stream == null) {
								((HttpServletResponse)context.getResponse()).setStatus(401);
								isDone = true;
							}
							else if (stream.size() > 0) {
								try {
									synchronized (stream) {
										stream.writeTo(context.getResponse().getOutputStream());
										stream.reset();
									}

									isDone = true;
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}

						context.complete();
					}

				}).start();
			} 
		}
		else {
			((HttpServletResponse)context.getResponse()).setStatus(401);
			context.complete();
		}
	}
}