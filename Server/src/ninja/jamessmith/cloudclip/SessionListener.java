/**
 * 
 */
package ninja.jamessmith.cloudclip;

import java.util.UUID;
import java.util.logging.Logger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * SessionListener responds to HttpSession events for when they are created and
 * destroyed. A randomly generated uuid is generated for the session during
 * creation. Once a session is destroyed, it checks if there is a sessionKey in
 * use by the session and instructs the SessionManager to clear the uuid.
 */
@WebListener
public class SessionListener implements HttpSessionListener {

	private SessionManager manager;

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		manager = SessionManager.getInstance();
		arg0.getSession().setAttribute("uuid", UUID.randomUUID().toString());
		
		Logger.getAnonymousLogger().info("session created " + arg0.getSession().getAttribute("uuid"));
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		HttpSession session = arg0.getSession();
		String uuid = (String) session.getAttribute("uuid");
		Object sessionKey = session.getAttribute("sessionKey");
		
		if (sessionKey != null) {
			manager.removeSession((String)sessionKey, uuid);
			
			Logger.getAnonymousLogger().info("sessionKey removed");
		}
		
		Logger.getAnonymousLogger().info("session destroyed");
	}
}
